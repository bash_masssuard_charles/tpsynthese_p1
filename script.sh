#!/bin/bash

#Ce script va demander le nom (avec son chemin absolu ou relatif) d'un fichier d'une arborescence.
#Ce script va copier ce fichier dans la racine du home en changeant ses droits en rw_r__r__
#Rajoute .SAV au nom du nouveau fichier

echo Choisissez un fichier à copier :
read fic


if [ -f $fic ]
    then 
        echo $fic existe !
        cp $fic ~/
        fic=${fic##*/}
        echo $fic a été copié à la racine de votre home !
        chmod u+rw,g=r,o=r ~/$fic
        echo Les droits de $fic ont été changés !
        mv ~/$fic ~/$fic.SAV
        echo $fic a été renommé en $fic.SAV !
    else
        echo $fic n\'existe pas !
fi
